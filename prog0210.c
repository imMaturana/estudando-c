#include <stdio.h>

int main()
{
    char c;

    printf("Digite um caractere: ");
    scanf("%c", &c);

    printf("O caractere digitado foi: %c\n", c);

    return 0;
}

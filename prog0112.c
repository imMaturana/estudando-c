#include <stdio.h>

/* A grande diferença entre `printf` e `puts`
 * está na formatação, `puts` não formata a string,
 * aceitando apenas um argumento.
 *
 * `puts` por padrão insere um \n no final da string.
 */

int main()
{
    puts("Hello, World");
    return 0;
}

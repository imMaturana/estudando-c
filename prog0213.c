#include <stdio.h>

int main()
{
    char c;

    printf("Digite um caractere: ");
    scanf("%c", &c);

    printf("O caractere '%c' tem o ASCII nº %d\n", c, (int) c);

    return 0;
}
#include <stdio.h>

int main()
{
    float quilos = 1.0E3;
    double gramas = 1.0e6;
    float n_toneladas, res_quilos, res_gramas;

    printf("Quantas toneladas comprou? ");
    scanf("%f", &n_toneladas);

    res_quilos = n_toneladas * quilos;
    res_gramas = n_toneladas * gramas;

    printf("Número de quilos = %f = %e\n", res_quilos, res_quilos);
    printf("Número de gramas = %f = %e\n", res_gramas, res_gramas);

    return 0;
}

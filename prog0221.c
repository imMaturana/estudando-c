#include <stdio.h>

int main()
{
    int d, m, y;

    printf("Digite uma data no formato aaaa-mm-dd: ");
    scanf("%04d-%02d-%02d", &y, &m, &d);

    printf("A data digitada foi: %02d/%02d/%04d\n", d, m, y);

    return 0;
}
#include <stdio.h>

/* Escreva um programa que calcule o Salário Bruto, o Salário Líquido e o
 * Imposto a pagar seguindo a seguinte regra:
 *
 *       Salário          Taxa
 *       < 1000            5%
 *  >= 1000 e < 5000       11%
 *      >= 5000            35%
 */

int main()
{
    float salario, imposto, sal_liquido;

    printf("Informe seu salário: R$");
    scanf("%f", &salario);

    if (salario < 1000)
        imposto = salario * 5 / 100;
    if (1000 <= salario < 5000)
        imposto = salario * 11 / 100;
    else
        imposto = salario * 35 / 100;

    sal_liquido = salario - imposto;

    printf("Salário Bruto: R$%.2f\n", salario);
    printf("Salário Líquido: R$%.2f\n", sal_liquido);
    printf("Impostos: R$%.2f\n", imposto);

    return 0;
}

#include <stdio.h>

/* Implemente um programa que adicione R$1.000,00 ao salário de um indíviduo,
 * caso este seja inferior a R$100.000,00.
 */

int main()
{
    float salario;

    printf("Digite seu salário: R$");
    scanf("%f", &salario);

    if (salario < 100000)
        salario = salario + 1000;
    
    printf("Salário final: R$%.2f\n", salario);

    return 0;
}

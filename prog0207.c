#include <stdio.h>

int main()
{
    float raio, perimetro;
    double PI = 3.1415927, area;

    printf("Introduza o raio da circunferência: ");
    scanf("%f", &raio);
    
    area = PI * raio * raio;
    perimetro = 2 * PI * raio;

    printf("Área = %.2f\nPerímetro = %.2f\n", area, perimetro);

    return 0;
}

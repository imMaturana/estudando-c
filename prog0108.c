#include <stdio.h>

int main()
{
    printf("Caracteres Especials\n\n");
    printf("\\n -\tNova linha\n");
    printf("\\t -\t<Tab>\n");
    printf("\\ -\tCaractere de escape\n");
    printf("%%%% -\tLiteral \"%\"\n");
    return 0;
}

#include <stdio.h>

int main()
{
    int num;

    printf("Digite um número de 0 a 255: ");
    scanf("%d", &num);

    printf("O número %d corresponde ao caractere '%c'\n", num, (char) num);
    printf("O caractere seguinte '%c' tem o ASCII de número %d\n", (char) num+1, num+1);
    
    return 0;
}
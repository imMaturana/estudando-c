#include <stdio.h>

int main()
{
    int d, m, y;

    printf("Digite um dia, mês e ano (formato numeral): ");
    scanf("%d%d%d", &d, &m, &y);

    printf("A data inserida foi: %02d/%02d/%04d\n", d, m, y);

    return 0;
}
#include <stdio.h>

int main()
{
    char c1, c2;

    printf("Digite um caractere: ");
    scanf("%c", &c1);

    printf("Digite outro caractere: ");
    scanf(" %c", &c2);

    printf("Os caracteres digitados foram: '%c' e '%c'\n", c1, c2);

    return 0;
}
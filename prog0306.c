#include <stdio.h>

/* Escreva um programa que solicite um salário ao utilizador e mostre o imposto
 * a pagar.
 *
 * - Se o salário for negativo ou zero mostre o erro respectivo.
 *
 * - Se o salário for maior que 1000, paga 10% de imposto, se não paga apenas 5%.
 */

int main()
{
    float salario;

    printf("Digite seu salário: R$");
    scanf("%f", &salario);

    if (salario <= 0)
        printf("Valor inválido!\n");
    else
        if (salario > 1000)
            printf("O imposto a pagar é de R$%.2f\n", salario * 10 / 100);
        else
            printf("O imposto a pagar é de R$%.2f\n", salario * 5 / 100);

    return 0;
}

#include <stdio.h>

int main()
{
    char c;

    printf("Digite um caractere: ");
    c = getchar();
    printf("O caractere digitado foi: %c\n", c);

    return 0;
}

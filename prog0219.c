#include <stdio.h>

int main()
{
    float num;

    printf("Digite um número real: ");
    scanf("%f", &num);

    printf("Parte inteira: %d\n", num);
    printf("Parte fracionária: %.2f\n", num - ((int) num));

    return 0;
}
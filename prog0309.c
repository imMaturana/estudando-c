#include <stdio.h>

/* Escreva um programa que aplique uma taxa de imposto de 10% aos solteiros e
 * de 9% aos casados.
 */

int main()
{
    float salario, imposto;
    char estado_civil;

    printf("Digite seu estado civil e salário respectivamente: ");
    scanf("%c%f", &estado_civil, &salario);

    if (estado_civil == 'C' || estado_civil == 'c')
        imposto = salario * 10 / 100;
    if (estado_civil == 'S' || estado_civil == 's')
        imposto = salario * 9 / 100;

    printf("Imposto a pagar: R$%.2f\n", imposto);

    return 0;
}

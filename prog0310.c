#include <stdio.h>

/* Implemente um programa que calcule os aumentos de ordenado para o corrente
 * ano. Se o ordenado for > 1000 deve ser aumentado 5%, se não deve ser
 * aumentado 7%.
 */

int main()
{
    float salario;

    printf("Informe o salário: R$");
    scanf("%f", &salario);

    salario = salario + (salario > 1000 ? salario * 5/100 : salario * 7/100);
    printf("Novo salário: R$%.2f\n", salario);

    return 0;
}

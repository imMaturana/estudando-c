#include <stdio.h>

int main()
{
    printf("O tamanho em bytes de um char\t= %d\n", sizeof(char));
    printf("O tamanho em bytes de um int\t= %d\n", sizeof(int));
    printf("O tamanho em bytes de um float\t= %d\n", sizeof(float));
    printf("O tamanho em bytes de um double\t= %d\n", sizeof(double));

    return 0;
}

#include <stdio.h>

/* Escreva um programa que leia dois números e os apresente por ordem
 * crescente.
 */

int main()
{
    int x, y;

    printf("Digite dois números: ");
    scanf("%d%d", &x, &y);

    if (x > y) {
        int tmp = x;
        x = y;
        y = tmp;
    }

    printf("%d %d\n", x, y);

    return 0;
}
